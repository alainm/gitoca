cmake_minimum_required(VERSION 3.5)

project("GitOCA" CXX)

enable_language(CXX)
include_directories(include)
add_executable(tex src/main.cpp src/tex.cpp)

